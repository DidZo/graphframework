package GraphAlgorithms;


public class BinaryHeap {

    private int[] nodes;
    private int pos;

    public BinaryHeap() {
        this.nodes = new int[32];
        for (int i = 0; i < nodes.length; i++) {
            this.nodes[i] = Integer.MAX_VALUE;
        }
        this.pos = 0;
    }

    public void resize() {
        int[] tab = new int[this.nodes.length + 32];
        for (int i = 0; i < nodes.length; i++) {
            tab[i] = Integer.MAX_VALUE;
        }
        System.arraycopy(this.nodes, 0, tab, 0, this.nodes.length);
        this.nodes = tab;
    }

    public boolean isEmpty() {
        return pos == 0;
    }

    public void insert(int element) {
        nodes[pos] = element;
        int currentIndex = pos;
        int parentIndex = (pos-1)/2;
        while (element < nodes[parentIndex]){
            swap(currentIndex, parentIndex);
            currentIndex = parentIndex;
            parentIndex = (parentIndex-1)/2;
        }
        pos += 1;
    }

    public int remove() {
        int currentIndex = 0;
        swap(pos-1, currentIndex);
        pos--;
        System.out.println(this);
        int bestChildIndex = getBestChildPos(currentIndex);
        while(bestChildIndex != 0){
            swap(currentIndex, bestChildIndex);
            System.out.println(this);
            currentIndex = bestChildIndex;
            bestChildIndex = getBestChildPos(currentIndex);
        }
        return 0;
    }

    private int getBestChildPos(int src) {
        if (isLeaf(src)) { // the leaf is a stopping case, then we return a default value
            return 0;
        } else {
            int childIndex = src*2+1;
            if (childIndex < pos-1 && nodes[childIndex+1] < nodes[childIndex]) {
                return childIndex+1; 
            }
            return childIndex;
        }
    }

    
    /**
	 * Test if the node is a leaf in the binary heap
	 * 
	 * @returns true if it's a leaf or false else
	 * 
	 */	
    private boolean isLeaf(int src) {
    	return (src * 2) + 1 >= pos;
    }

    private void swap(int father, int child) {
        int temp = nodes[father];
        nodes[father] = nodes[child];
        nodes[child] = temp;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < pos; i++) {
            s.append(nodes[i]).append(", ");
        }
        return s.toString();
    }

    /**
	 * Recursive test to check the validity of the binary heap
	 * 
	 * @returns a boolean equal to True if the binary tree is compact from left to right
	 * 
	 */
    public boolean test() {
        return this.isEmpty() || testRec(0);
    }

    private boolean testRec(int root) {
        if (isLeaf(root)) {
            return true;
        } else {
            int left = 2 * root + 1;
            int right = 2 * root + 2;
            if (right >= pos) {
                return nodes[left] >= nodes[root] && testRec(left);
            } else {
                return nodes[left] >= nodes[root] && testRec(left) && nodes[right] >= nodes[root] && testRec(right);
            }
        }
    }

    public static void main(String[] args) {
        BinaryHeap jarjarBin = new BinaryHeap();
        System.out.println(jarjarBin.isEmpty()+"\n");
        int k = 10;
        int m = k;
        int min = 2;
        int max = 100;
        while (k > 0) {
            int rand = min + (int) (Math.random() * ((max - min) + 1));
            System.out.print("insert " + rand);
            System.out.println("\n" + jarjarBin);
            System.out.println();
            jarjarBin.insert(rand);            
            k--;
        }

        System.out.println(jarjarBin);
        System.out.println("start remove");
        jarjarBin.remove();
        System.out.println("end remove");
        System.out.println(jarjarBin);
    }

}
