package GraphAlgorithms;

import java.util.*;

import AdjacencyList.DirectedValuedGraph;
import AdjacencyList.UndirectedGraph;
import AdjacencyList.UndirectedValuedGraph;
import AdjacencyMatrix.AdjacencyMatrixDirectedValuedGraph;
import Collection.Triple;
import Nodes.DirectedNode;
import Nodes.UndirectedNode;

public class GraphToolsList extends GraphTools {

    public static List<UndirectedNode> debut; 
    public static List<UndirectedNode> fin;
    public static final int MAX_VALUE = 999999999;

    // --------------------------------------------------
    // Constructors
    // --------------------------------------------------

    public GraphToolsList() {
        super();
    }

    // ------------------------------------------
    // Methods
    // ------------------------------------------
    
    /**
     * Exploration en profondeur d'un graphe non orienté
     * @param g : UndirectedGraph
     */
    public static void dfs(UndirectedGraph g) {
        System.out.println("DFS START");
        debut = new ArrayList<>();
        fin = new ArrayList<>();
        for (UndirectedNode node : g.getNodes()) {
            if (!debut.contains(node)) {
                explorerDfs(node);
                fin.add(node);
            }
        }
        System.out.println("tableau debut visite : "+ debut);
        System.out.println("tableau fin visite : "+ fin);
        System.out.println("DFS END");
        System.out.println();
    }

    /**
     * Exploration d'un noeud de graphe non orienté
     * @param node : UndirectedNode
     */
    public static void explorerDfs(UndirectedNode node) {
        debut.add(node);
        System.out.println("visit " + node);
        for (UndirectedNode neigh : node.getNeighbours().keySet()) {
            if (!debut.contains(neigh)) {
                explorerDfs(neigh);
                fin.add(neigh);
            }
        }
    }



    /**
     * Exploration itérative d'un graphe non orienté en largeur  
     * @param g : UndirectedGraph
     */
    public static void bfs(UndirectedGraph g) {
        System.out.println("BFS START");
        LinkedList<UndirectedNode> queue = new LinkedList<>();
        debut = new ArrayList<>();
        UndirectedNode tmpNode;
        for (UndirectedNode node : g.getNodes()) {
            if (!debut.contains(node)) {
                tmpNode = node;
                queue.addLast(tmpNode);
                debut.add(tmpNode);
                while (!queue.isEmpty()) {
                    tmpNode = queue.removeLast();
                    System.out.println("visited " + tmpNode);
                    for (UndirectedNode visitedNode : tmpNode.getNeighbours().keySet()) {
                        if (!debut.contains(visitedNode)) {
                            queue.addLast(visitedNode);
                            debut.add(visitedNode);
                        }
                    }
                }
            }
        }
        System.out.println("tableau debut visite : "+ debut);
        System.out.println("BFS END");
        System.out.println("");
    }

    /**
     * Exemple d'implémentation de l'algorithme de Dijkstra
     * @param start : starting node 
     * @param graph : AdjacencyMatrixDirectedValuedGraph  
     */
    public static void dijkstra(int start, AdjacencyMatrixDirectedValuedGraph graph) {
        System.out.println("Dijkstra START");
        List<Integer> nodes = new ArrayList<>();
        List<Integer> distances = new ArrayList<>();
        List<Boolean> visited = new ArrayList<>();
        List<Integer> predecesseurs = new ArrayList<>();

        int n = 0;
        for (int i = 0; i < graph.getNbNodes(); i++) {
            visited.add(false);
            nodes.add(-1);
            predecesseurs.add(-1);
            distances.add(Integer.MAX_VALUE);
        }

        nodes.set(n, start);
        distances.set(start,0);
        predecesseurs.set(start,start); 
        visited.set(start, true); 

        DirectedNode startNode = new DirectedNode(start);
        for (int i = 0; i < graph.getNbNodes(); i++) {
            DirectedNode nodeI = new DirectedNode(i);
            if (graph.isArc(startNode, nodeI)) {
                distances.set(i, graph.getMatrixCosts()[start][i]);
                predecesseurs.set(i, start);
            } else {
                if (start != i)
                    distances.set(i, Integer.MAX_VALUE);
            }
        }

        for (int i = 0; i < graph.getNbNodes(); i++) {
            if (!visited.get(i)) {
                int sommet = choixSommet(start, graph, visited, distances);
                visited.set(sommet, true);
                n++;
                nodes.set(n, sommet);
                for (int j = 0; j < graph.getNbNodes(); j++) {
                    if (!visited.get(j)) {
                        if ((distances.get(sommet) + (graph.isArc(new DirectedNode(sommet), new DirectedNode(j))
                                ? graph.getMatrixCosts()[sommet][j]
                                : 1000)) < distances.get(j)) {

                            distances.set(j, distances.get(sommet)        + (graph.isArc(new DirectedNode(sommet), new DirectedNode(j))
                                            ? graph.getMatrixCosts()[sommet][j]
                                            : 1000));
                            predecesseurs.set(j, sommet);
                        }
                    }
                }

            }
        }
        System.out.println("Distances : "+ distances);
        System.out.println("Predecesors : "+ predecesseurs);
        System.out.println("Dijkstra END");
        System.out.println();
    }

    /**
     * Exemple d'implémentation de l'algorithme de ford bellman 
     * @param graphe : AdjacencyMatrixDirectedValuedGraph
     * @param startingNode : int
     */
    public static void bellman(AdjacencyMatrixDirectedValuedGraph graphe, int startingNode) {
        System.out.println("Bellman START");

        int nbNodes = graphe.getNbNodes();

        int[][] adjacencyMatrix = graphe.getMatrixCosts();
        List<Integer> distances = new ArrayList<>();
        List<Integer> ancessors = new ArrayList<>();

        for (int vertex = 0; vertex < nbNodes; vertex++) {
            distances.add(MAX_VALUE);
            ancessors.add(-1);
        }

        //INITIALISATION DE LA NODE DE DEPART
        distances.set(startingNode, 0);
        ancessors.set(startingNode, startingNode);

        for (int sommet = 0; sommet < nbNodes; sommet++) {
            for (int start = 0; start < nbNodes; start++) {
                for (int destinationNode = 0; destinationNode < nbNodes; destinationNode++) {
                    if (adjacencyMatrix[start][destinationNode] != MAX_VALUE) {
                        if (distances.get(destinationNode) > distances.get(start)
                                + adjacencyMatrix[start][destinationNode]) {
                            distances.set(destinationNode, distances.get(start) + adjacencyMatrix[start][destinationNode]);
                            ancessors.set(destinationNode, start);
                        }
                    }
                }
            }
        }

        for (int start = 0; start < nbNodes; start++) {
            for (int destinationNode = 0; destinationNode < nbNodes; destinationNode++) {
                if (adjacencyMatrix[start][destinationNode] != MAX_VALUE) {
                    if (distances.get(destinationNode) > distances.get(start) + adjacencyMatrix[start][destinationNode]) {
                        System.out.println("Le graphe contient un cycle négatif");
                    }
                }
            }
        }

        for (int sommet = 0; sommet < nbNodes; sommet++) {
            System.out.println(
                    "Distance du sommet de début  " + startingNode + " au sommet " + sommet + " est " + distances.get(sommet));
        }
        System.out.println("Distances");
        System.out.println(distances);
        System.out.println("Prédecesseurs");
        System.out.println(ancessors);
        System.out.println("Bellman END \n");
    }

    
    /**
     * Exemple d'implémentation de l'algorithme avec listes d'adjacences 
     * @param graphe
     * @param debutIndex
     * @return Liste de Triple représentant un arbre couvrant minimal de graphe
     */
    public static List<Triple<DirectedNode,DirectedNode,Integer>> prim(DirectedValuedGraph graphe, int debutIndex) {
	System.out.println("simple Prim START");
        List<Triple<DirectedNode,DirectedNode,Integer>> arbre = new ArrayList<>();
        List<DirectedNode> sommetsVisites = new ArrayList<>();
        boolean finished = false;
	int cout = 0;
        DirectedNode sommet = graphe.getNodes().get(debutIndex);
        sommetsVisites.add(sommet);
        while (!finished) {
            int coutMin = Integer.MAX_VALUE;

            Triple<DirectedNode,DirectedNode,Integer> arcAjoute = null;
            for (DirectedNode sommetVisite : sommetsVisites) {
                for (DirectedNode voisin : sommetVisite.getSuccs().keySet()) {
                    if (!sommetsVisites.contains(voisin) && sommetVisite.getSuccs().get(voisin) < coutMin) {
                        coutMin = sommetVisite.getSuccs().get(voisin);

                        arcAjoute = new Triple<>(sommetVisite, voisin, coutMin);
                    }
                }
            }

            System.out.println(arcAjoute.getFirst() + " -> " + arcAjoute.getSecond() + " : " + arcAjoute.getThird());
            cout+= arcAjoute.getThird();
            arbre.add(arcAjoute);
            sommetsVisites.add(arcAjoute.getSecond());

            if (sommetsVisites.size() == graphe.getNodes().size()) {
                finished = true;
            }
        }
        System.out.println("Cout : " + cout);
	    System.out.println("simple Prim END");
        return arbre;
    }

    /**
     * Debut de l'implémentation de Prim avec le tas binaire
     * @param graphe
     * @param debutIndex 
     * @return
     */
    // public static void primBinaryHeapEdge(UndirectedValuedGraph graphe, int debutIndex) {
    //     BinaryHeapEdge tas = new BinaryHeapEdge();
    //     List<UndirectedNode> sommetsVisites = new ArrayList<>();
    //     int cout = 0;

    //     UndirectedNode start = graphe.getNodes().get(debutIndex);

    //     for (UndirectedNode voisin : start.getNeighbours().keySet()) {
    //         if (!sommetsVisites.contains(voisin)) {
    //             tas.insert(start, voisin, start.getNeighbours().get(voisin));
    //             sommetsVisites.add(voisin);
    //         }
    //     }
    // }
    // 

    /**
     * Choix du meilleur sommet enfant non visité dans un graphe 
     * @param start
     * @param graph
     * @param visited
     * @param distances
     * @return
     */
    private static int choixSommet(int start, AdjacencyMatrixDirectedValuedGraph graph, List<Boolean> visited, List<Integer> distances) {
        int valeurMin = Integer.MAX_VALUE;
        int min = start;
        for (int i = 0; i < graph.getNbNodes(); i++) {
            if (!visited.get(i)) {
                if (distances.get(i) <= valeurMin) {
                    min = i;
                    valeurMin = distances.get(i);
                }
            }
        }
        return min;
    }

    public static void main(String[] args) {

        int[][] matrix = GraphTools.generateGraphData(10, 20, false, false, true, 100001);
        GraphTools.afficherMatrix(matrix);
        UndirectedGraph al = new UndirectedGraph(matrix);
        dfs(al);
        bfs(al);

        int[][] m = {
                { 0, 1, 1, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 1, 0, 0, 0, 1 },
                { 0, 1, 0, 0, 0, 1, 1, 0 },
                { 0, 0, 1, 0, 1, 0, 1, 0 },
                { 0, 1, 0, 0, 0, 0, 0, 1 },
                { 0, 0, 0, 1, 1, 0, 0, 0 },
                { 1, 0, 0, 0, 0, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 1, 0, 0 }
        };

        int[][] mCost = {
                { 0, 2, 6, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 1, 0, 0, 0, 1 },
                { 0, 3, 0, 0, 0, 2, 2, 0 },
                { 0, 0, 2, 0, 7, 0, 6, 0 },
                { 0, 3, 0, 0, 0, 0, 0, 2 },
                { 0, 0, 0, 1, 4, 0, 0, 0 },
                { 1, 0, 0, 0, 0, 2, 0, 0 },
                { 0, 0, 0, 0, 0, 3, 0, 0 }
        };
        AdjacencyMatrixDirectedValuedGraph graph = new AdjacencyMatrixDirectedValuedGraph(m, mCost);
        dijkstra(0, graph);

        int[][] m2Cost = {
                { 999, 4, 999, 2, 999, 999 },
                { 999, 999, 15, -6, 4, 999 },
                { 999, 999, 999, 999, 999, 1 },
                { 999, 999, 999, 999, 2, 999 },
                { 999, 999, 999, 999, 999, 17 },
                { 999, 999, 999, 999, 999, 999 },
        };
        int[][] m2 = {
                { 0, 1, 1, 0, 0, 0 },
                { 0, 0, 0, 0, 1, 1 },
                { 0, 0, 0, 1, 1, 0 },
                { 0, 0, 0, 0, 0, 1 },
                { 0, 0, 0, 0, 0, 1 },
                { 0, 0, 1, 0, 0, 0 },
        };

        // Exemple de trace donné sur moodle
        // Résultat : 26 , OK
        int[][] m3 = {
                { 0, 4, 0, 6, 0, 0, 2, 0 },
                { 4, 0, 2, 0, 5, 0, 0, 0 },
                { 0, 2, 0, 7, 0, 0, 0, 5 },
                { 6, 0, 7, 0, 0, 8, 0, 0 },
                { 0, 5, 6, 0, 0, 0, 4, 0 },
                { 0, 0, 0, 8, 0, 0, 7, 3 },
                { 2, 0, 0, 0, 4, 7, 0, 5 },
                { 0, 0, 5, 0, 0, 3, 5, 0 },
        };

        AdjacencyMatrixDirectedValuedGraph graph2 = new AdjacencyMatrixDirectedValuedGraph(m2, m2Cost);
        bellman(graph2, 0);

        DirectedValuedGraph graph3 = new DirectedValuedGraph(m3);
        prim(graph3, 4);
    }
}
