# Graph-Framework

### Membres

- IMENSAR Yan ➡️ yan.imensar@imt-atlantique.net
- CALOONE Cantin ➡️ cantin.caloone@imt-atlantique.net
- ORVOEN Romain ➡️ romain.orvoen@imt-atlantique.net

### Exécution du code

L'ensemble du code réalisé dans le cadre de l'évaluation se situe dans la classe GraphToolsList ('src/main/java/GraphAlgorithms/GraphToolsList.java') et peut être exécuté dans le main.
Il contient:
- Le DFS
- Le BFS
- Dijkstra 
- Bellman-Ford
- Prim (la tentative d'implémentation du tas binaire dans le cadre de cet algorithme n'a pas abouti et a dûe être abandonnée)

### Réponses aux questions des TDs

#### TD-TP4 ➡️ Question 5
Dans le pire des cas notre algorithme a une complexité de 'O(n^2)' à cause des multiples parcours de la structure de donnée.
Dans le cadre d'une implémentation avec un BinaryHeapEdge serait de 'O(log n)'.


